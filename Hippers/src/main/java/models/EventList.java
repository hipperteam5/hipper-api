/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import Exceptions.InvalidIdException;
import java.util.ArrayList;
import java.util.List;
import models.mobile.MobileEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import services.AssignedExerciseService;
import services.ExerciseService;

/**
 *
 * @author mats
 */

public class EventList {



    private List<Event> events;

    public EventList(List<Event> events) throws InvalidIdException {
        setEvents(events);
    }
    

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> list) throws InvalidIdException {
        this.events = list;
    }

}
