/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author yorin
 */
@Document(collection = "Therapists")
public class Therapist {

    @Id
    String id;

    @Indexed(unique = true) //makes sure username is unique
    private String username;

    private List<String> roles = new ArrayList();
    private String firstName;
    private String lastName;
    private String phoneNr;
    private String therapistType;

    public Therapist() {
    }

    public Therapist(String username, String firstName, String lastName, String phoneNr, String therapistType) {
        this.username = username;
        if (roles.size() == 0) {
            this.roles.add("THERAPIST");
        }
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNr = phoneNr;
        this.therapistType = therapistType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNr() {
        return phoneNr;
    }

    public void setPhoneNr(String phoneNr) {
        this.phoneNr = phoneNr;
    }

    public String getTherapistType() {
        return therapistType;
    }

    public void setTherapistType(String therapistType) {
        this.therapistType = therapistType;
    }

}
