/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;


import javax.persistence.Id;
import org.joda.time.DateTime;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Yorin
 */
@Document(collection = "Hash")
public class Hash {

    @Id
    private String id;

    @Indexed(unique = true, sparse = true)
    private String adminId;
    
    @Indexed(unique = true, sparse = true)
    private String therapistId;
    
    @Indexed(unique = true, sparse = true)
    private String patientId;

    private String hash;
    
    DateTime date = DateTime.now();

    public Hash(String adminId, String therapistId, String patientId, String hash) {
        this.adminId = adminId;
        this.therapistId = therapistId;
        this.patientId = patientId;
        this.hash = hash;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }


    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

   

    
}
