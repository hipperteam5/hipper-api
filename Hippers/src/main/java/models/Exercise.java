/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.persistence.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Maarten
 */
@Document(collection = "Exercises")
public class Exercise {

    @Id
    String id;

    @Indexed(unique = true)
    private String exerciseName;

    private String therapistId;
    private String youtubelink;
    private int repetitions;
    private String description;

    public Exercise() {
    }

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public String getExerciseName() {
	return exerciseName;
    }

    public void setExerciseName(String exerciseName) {
	this.exerciseName = exerciseName;
    }

    public String getTherapistId() {
	return therapistId;
    }

    public void setTherapistId(String therapistId) {
	this.therapistId = therapistId;
    }

    public String getYoutubelink() {
	return youtubelink;
    }

    public void setYoutubelink(String youtubelink) {
	this.youtubelink = youtubelink;
    }

    public int getRepetitions() {
	return repetitions;
    }

    public void setRepetitions(int repetitions) {
	this.repetitions = repetitions;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

}
