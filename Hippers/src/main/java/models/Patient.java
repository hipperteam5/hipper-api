/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author yorin
 */
@Document(collection = "Patients")
public class Patient {

    @Id
    private String id;

    @Indexed(unique = true)
    private String patientNr;

    private String firstName;
    private String lastName;
    private String email;
    private boolean active = true;
    private List<String> roles = new ArrayList();
    private List<String> therapistId = new ArrayList<>();

    public Patient() {
        if (roles.isEmpty()) {
            this.roles.add("PATIENT");
        }
    }

    public String getPatientNr() {
        return patientNr;
    }

    public void setPatientNr(String patientNr) {
        this.patientNr = patientNr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<String> getTherapistId() {
        return therapistId;
    }

    public void setTherapistId(List<String> therapistId) {
        this.therapistId = therapistId;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
