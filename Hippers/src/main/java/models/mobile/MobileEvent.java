/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.mobile;

import models.AssignedExercise;
import models.Event;
import models.Exercise;

/**
 *
 * @author mats
 */
public class MobileEvent {
    private String id;
    private Exercise exercise;
    private AssignedExercise assigned;
    private Event event;

    public MobileEvent(Exercise exercise, AssignedExercise assigned, Event event) {
        this.id = assigned.getId();
        this.exercise = exercise;
        this.assigned = assigned;
        this.event = event;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Exercise getExercise() {
        return exercise;
    }

    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
    }

    public AssignedExercise getAssigned() {
        return assigned;
    }

    public void setAssigned(AssignedExercise assigned) {
        this.assigned = assigned;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public String toString() {
        return "MobileEvent{" + "id=" + id + ", exercise=" + exercise + ", assigned=" + assigned + ", event=" + event + '}';
    }
    
    
    
}
