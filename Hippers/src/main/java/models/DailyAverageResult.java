/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Maarten Petit
 */
public class DailyAverageResult {

    private String date;
    private int resultColor;
    //color that the agenda will display
    //1 = good, 2 = yellow(ok), 3 = red(bad)

    public DailyAverageResult(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getResultColor() {
        return resultColor;
    }

    public void setResultColor(int resultColor) {
        this.resultColor = resultColor;
    }

}
