/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.persistence.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author yorin
 */
@Document(collection = "ExerciseData")
public class ExerciseData {

    @Id
    String id;

    private String assignedExerciseId;

    private double[] x;
    private double[] y;
    private double[] z;
    private long[] timestamp;
    private int runId;

    public ExerciseData(double[] x, double[] y, double[] z, long[] timestamp, int runId) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.timestamp = timestamp;
        this.runId = runId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double[] getX() {
        return x;
    }

    public void setX(double[] x) {
        this.x = x;
    }

    public double[] getY() {
        return y;
    }

    public void setY(double[] y) {
        this.y = y;
    }

    public double[] getZ() {
        return z;
    }

    public void setZ(double[] z) {
        this.z = z;
    }

    public long[] getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long[] timestamp) {
        this.timestamp = timestamp;
    }

    public int getRunId() {
        return runId;
    }

    public void setRunId(int runId) {
        this.runId = runId;
    }

    public String getAssignedExerciseId() {
        return assignedExerciseId;
    }

    public void setAssignedExerciseId(String assignedExerciseId) {
        this.assignedExerciseId = assignedExerciseId;
    }

}
