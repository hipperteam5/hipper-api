/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Maarten Petit
 */
public class Event {

    String assignedExerciseId;              //id of linked AssignedExercise
    private String title;                   //title of event is title of exercise + reps.
    private String start;                   //date of event format:(yyyy-mm-dd).
    private int color;                      //color of event

    private final boolean allDay = true;    //needed for clientside agenda, always true!

    public Event() {
    }

    public Event(String assignedExerciseId, String title, String start, int color) {
        this.assignedExerciseId = assignedExerciseId;
        this.title = title;
        this.start = start;
        this.color = color;
    }

    public String getAssignedExerciseId() {
        return assignedExerciseId;
    }

    public void setAssignedExerciseId(String assignedExerciseId) {
        this.assignedExerciseId = assignedExerciseId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
