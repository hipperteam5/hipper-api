/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author mats
 */
public class JwtTherapistTokenResult extends JwtTokenResult {
    private Therapist user;

    
    public JwtTherapistTokenResult(String hipperAccessToken, Therapist therapist) {
        super(hipperAccessToken);
        this.user = therapist;
    }
    
    public Therapist getUser() {
        return user;
    }

    public void setUser(Therapist user) {
        this.user = user;
    }
    
    

}
