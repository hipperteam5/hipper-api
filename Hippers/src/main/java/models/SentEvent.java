/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Maarten
 */
public class SentEvent {

    private String exerciseId;
    private String start;
    private int repetitions;
    private int color;

    public SentEvent() {
    }

    public String getExerciseId() {
	return exerciseId;
    }

    public void setExerciseId(String ExerciseId) {
	this.exerciseId = ExerciseId;
    }

    public String getStart() {
	return start;
    }

    public void setStart(String start) {
	this.start = start;
    }

    public int getRepetitions() {
	return repetitions;
    }

    public void setRepetitions(int repetitions) {
	this.repetitions = repetitions;
    }

    public int getColor() {
	return color;
    }

    public void setColor(int color) {
	this.color = color;
    }

}
