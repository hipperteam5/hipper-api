/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author mats
 */
public class JwtPatientTokenResult extends JwtTokenResult {
    private Patient user;

    
    public JwtPatientTokenResult(String hipperAccessToken, Patient patient) {
        super(hipperAccessToken);
        this.user = patient;
    }
    
     public Patient getUser() {
        return user;
    }

    public void setUser(Patient user) {
        this.user = user;
    }

}
