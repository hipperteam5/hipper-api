package models;

import java.util.ArrayList;

/**
 *
 * @author Maarten
 */
public class WeekGraphData {

    private String patientId;
    private int weeknumber;
    private ArrayList<WeeklyAverageResults> results;

    public WeekGraphData(String patientId, int weeknumber) {
        this.patientId = patientId;
        this.weeknumber = weeknumber;
        results = new ArrayList<>();
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public int getWeeknumber() {
        return weeknumber;
    }

    public void setWeeknumber(int weeknumber) {
        this.weeknumber = weeknumber;
    }

    public ArrayList<WeeklyAverageResults> getResults() {
        return results;
    }

    public void setResults(ArrayList<WeeklyAverageResults> results) {
        this.results = results;
    }

}
