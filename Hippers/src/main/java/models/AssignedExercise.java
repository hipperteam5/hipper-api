
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.persistence.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Maarten Petit
 */
@Document(collection = "AssignedExercises")
public class AssignedExercise {

    @Id
    String id;

    private String exerciseId;  //id of the exercise that is assigned to the patient
    private String patientId;   // id of the patient that the exercise is assigned to

    private int repetitions;    //amount of repetitions that the patient is required to do
    private int results;	//amount of repetitions done by the patient

    public AssignedExercise() {
    }

    public AssignedExercise(String exerciseId, String patientId, int repetitions) {
        this.exerciseId = exerciseId;
        this.patientId = patientId;
        this.repetitions = repetitions;
        this.results = 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExerciseId() {
        return exerciseId;
    }

    public void setExerciseId(String exerciseId) {
        this.exerciseId = exerciseId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public int getRepetitions() {
        return repetitions;
    }

    public void setRepetitions(int repetitions) {
        this.repetitions = repetitions;
    }

    public int getResults() {
        return results;
    }

    public void setResults(int results) {
        this.results = results;
    }

}
