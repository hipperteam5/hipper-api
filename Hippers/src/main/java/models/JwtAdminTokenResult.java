/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author mats
 */
public class JwtAdminTokenResult extends JwtTokenResult {
    private Admin user;

    
    public JwtAdminTokenResult(String hipperAccessToken, Admin admin) {
        super(hipperAccessToken);
        this.user = admin;
    }
    
     public Admin getUser() {
        return user;
    }

    public void setUser(Admin user) {
        this.user = user;
    }

}
