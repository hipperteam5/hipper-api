/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.List;

/**
 *
 * @author Maarten Petit
 */
public class ResultsAgenda {

    private String patientId;

    private List<DailyAverageResult> dailyAverageResults;

    public ResultsAgenda(String patientId, List<DailyAverageResult> dailyAverageResults) {
        this.patientId = patientId;
        this.dailyAverageResults = dailyAverageResults;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public List<DailyAverageResult> getDailyAverageResults() {
        return dailyAverageResults;
    }

    public void setDailyAverageResults(List<DailyAverageResult> dailyAverageResults) {
        this.dailyAverageResults = dailyAverageResults;
    }
}
