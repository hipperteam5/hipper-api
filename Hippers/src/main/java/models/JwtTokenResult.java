/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author mats
 */
public class JwtTokenResult {
    private String hipperAccessToken;

    public JwtTokenResult(String hipperAccessToken) {
        this.hipperAccessToken = hipperAccessToken;
        
    }
    

    public String getHipperAccessToken() {
        return hipperAccessToken;
    }

    public void setHipperAccessToken(String hipperAccessToken) {
        this.hipperAccessToken = hipperAccessToken;
    }

}
