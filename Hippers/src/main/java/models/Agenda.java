package models;

import Exceptions.InvalidIdException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Maarten Petit
 */
@Document(collection = "Agendas")
public class Agenda {

    @Id
    String id;

    @Indexed(unique = true)
    private String patientId;

    private List<Event> events = new ArrayList<>();     //List of events that are planned for the patient

    public Agenda(String patientId) {
        this.patientId = patientId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public void addEvent(Event event) {
        this.events.add(event);
    }

    public void deleteEvent(Event event) {
        this.events.remove(event);
    }

    /**
     * This method searches for an event that is coupled to the assignedExercise
     * whose id is given.
     *
     * @param assignedExerciseId id of the assignedExercise from the event you
     * need.
     * @return the event whose assignedExerciseId is given.
     * @throws InvalidIdException If there is no id with the assignedExerciseId
     * that is given.
     */
    public Event getEventByAssignedExerciseId(String assignedExerciseId) throws InvalidIdException {
        for (int i = 0; i < this.events.size(); i++) {
            if (this.events.get(i).getAssignedExerciseId().equals(assignedExerciseId)) {
                return this.events.get(i);
            }
        }
        throw new InvalidIdException();
    }

    /**
     * This method returns the ids of all AssignedExercises that are planned on
     * the given date.
     *
     * @param date The date whose planned AssignedExercises you need.
     * @return All the AssignedExercises planned on the given date.
     */
    public ArrayList<String> getAssignedExerciseIdByEventDate(String date) {
        ArrayList<String> assignedExerciseIds = new ArrayList<>();
        for (int i = 0; i < this.events.size(); i++) {
            if (this.events.get(i).getStart().equals(date)) {
                assignedExerciseIds.add(events.get(i).getAssignedExerciseId());
            }
        }
        return assignedExerciseIds;
    }
}
