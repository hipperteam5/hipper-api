/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Id;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author yorin & matzumatzu
 */
@Document(collection = "Admins")
public class Admin {

    @Id
    private String id;

    private List<String> roles = new ArrayList();

    @Indexed(unique = true)
    private String username;

    public Admin(String username) {
        this.username = username;
        if (roles.size() == 0) {
            this.roles.add("ADMIN");
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
