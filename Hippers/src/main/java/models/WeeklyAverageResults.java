package models;

/**
 *
 * @author Maarten Petit
 */
public class WeeklyAverageResults {

    private String exerciseId;
    private String exerciseName;
    private boolean targetAchieved;

    private int[] results;	//Number of repetitions done by patient in 1 day.
    private int[] planning;     //Number of repetitions assigned for 1 day.

    public WeeklyAverageResults(String exerciseId) {
        this.exerciseId = exerciseId;
        this.results = new int[7];
        this.planning = new int[7];
        this.targetAchieved = true;
    }

    public String getExerciseId() {
        return exerciseId;
    }

    public void setExerciseId(String exerciseId) {
        this.exerciseId = exerciseId;
    }

    public int[] getResults() {
        return results;
    }

    public void addToResults(int index, int results) {
        this.results[index] += results;
    }

    public int[] getPlanning() {
        return planning;
    }

    public void addToPlanning(int index, int repetitions) {
        this.planning[index] += repetitions;
    }

    public String getExerciseName() {
        return exerciseName;
    }

    public void setExerciseName(String exerciseName) {
        this.exerciseName = exerciseName;
    }

    public boolean isTargetAchieved() {
        return targetAchieved;
    }

    public void setTargetAchieved(boolean targetAchieved) {
        this.targetAchieved = targetAchieved;
    }

}
