/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.List;

/**
 *
 * @author mats
 */
public class Token {

    private String user_id;
    private String username;
    private List<String> roles;

    public Token(String user_id, String username, List<String> roles) {
        this.user_id = user_id;
        this.username = username;
        this.roles = roles;
    }

    public String getUser_id() {
        return user_id;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
    
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


}
