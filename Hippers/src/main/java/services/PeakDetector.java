/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

/**
 *
 * @author yorin
 */
public class PeakDetector {

    double[] vals;
    int windowSize;
    double h;
    int peaks = 0;
    int n;
    double[] a;
    private double s;
    private double m;
    DescriptiveStatistics ds = new DescriptiveStatistics();

    public PeakDetector(double[] vals, int windowSize, double h) {
        this.vals = vals;
        this.windowSize = windowSize;
        this.h = h;
        this.n = vals.length;
        this.a = new double[n];
    }

    public int findPeaks() {

        // calculate peak value for every value in vals
        for (int i = 0; i < n - windowSize; i++) {
            if (i < windowSize) {
                i++;
            } else {
                a[i] = peakValue(i);
//                System.out.println(i + " : " + a[i]);
            }
        }
//       get mean and standard deviation of all the values that are bigger than 0
        for (int i = 0; i < n; i++) {
            if (a[i] > 0) {
                ds.addValue(a[i]);
            }
        }
        s = ds.getStandardDeviation();
        m = ds.getMean();
//        System.out.println("M: " + m);
//        System.out.println("S: " + s);

        // why?
        m = 0.7 * m;

        // eliminate peaks that that don't have a high enough peak score
        double[] f = new double[n]; //all scores that were high enough
        int[] indices = new int[n]; // all indices of peaks that have a high enough score
        for (int i = 0; i < f.length; i++) {
            if (a[i] > 0 && (a[i] - m) > (h * s)) {
//                System.out.println(i + " : " + a[i]);
                f[i] = a[i];
                indices[i] = i;
            }

        }
        // remove peaks that are too close to each other. Always choose the highest peak
        for (int i = 0; i < f.length; i++) {
            int allowedDif = windowSize - 1;
            if (f[i] > 0) {
                for (int j = 1; j < allowedDif; j++) {
                    if ((i + j) > f.length - 1) {
                        break;
                    }
                    if (f[i + j] > 0) {
                        if (f[i] > f[i + j]) {
                            f[i + j] = 0;
                        } else {
                            f[i] = 0;
                        }
                    }
                }
            }
        }
        //sort (not needed?)

        // count peaks
        for (int i = 0; i < f.length; i++) {
            if (f[i] > 0) {
//                System.out.println("PEAK INDEX22222: " + indices[i]);
                peaks++;
            }
        }
        return peaks;
    }

    //calculate peak value by comparing the difference between the numbers around this number
    private double peakValue(int cur) {
        int v = windowSize * 2;
        double result;
        double[] x = new double[v];
        for (int i = 0, j = 0; i < v; i++, j++) {
            if (cur - windowSize + j == cur) {
                j++;
            }
            x[i] = vals[cur] - vals[cur - windowSize + j];
        }

        double minTotal = x[0];
        for (int i = 1; i < x.length - windowSize; i++) {
            minTotal += x[i];
        }
        double plusTotal = x[windowSize];
        for (int i = windowSize + 1; i < x.length; i++) {
            plusTotal += x[i];
        }
        minTotal = minTotal / windowSize;
        plusTotal = plusTotal / windowSize;

        // prevents a high score being given when most of that score comes just from one side. This would be 
        // a peak who only has a real valley on one side.
        if (minTotal <= (0.3 * plusTotal)) {
            return 0;
        } else if (plusTotal <= (0.3 * minTotal)) {
            return 0;
        }

        result = (plusTotal + minTotal) / 2;
        return result;
    }

}
