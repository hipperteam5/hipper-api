/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import Exceptions.InvalidIdException;
import Exceptions.AccessDeniedException;
import java.util.ArrayList;
import java.util.List;
import models.Agenda;
import models.AssignedExercise;
import models.Event;
import models.SentEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.AgendaRepository;

/**
 *
 * @author Maarten
 */
@Service
public class AgendaService {

    @Autowired
    private AgendaRepository agendaRepository;

    @Autowired
    private PatientService patientService;

    @Autowired
    private ExerciseService exerciseService;

    @Autowired
    private AssignedExerciseService assignedExerciseService;

    /**
     * This method creates an empty agenda and stores it in the database for the
     * patient whose id is given.
     *
     * @param patientId Id of patient for which an empty Agenda needs to be
     * created.
     */
    public void createAgenda(String patientId) {
        Agenda newAgenda = new Agenda(patientId);
        agendaRepository.save(newAgenda);
    }

    /**
     * This method returns the agenda of the patient whose id is given. This
     * specific method is used only when the agenda is needed for a user of the
     * webservice.
     *
     * @param patientId Id of the patient whose Agenda needs to be returned.
     * @return Agenda of patient whose id given.
     * @throws InvalidIdException If no patient with given id exists.
     * @throws AccessDeniedException If the current therapist is not assigned to
     * this patient.
     */
    public Agenda findAgendaByPatientIdForTherapist(String patientId) throws InvalidIdException, AccessDeniedException {
        Agenda agenda = findAgendaByPatientId(patientId);
        patientService.checkAuthorization(patientId);
        return agenda;
    }

    /**
     * This method return the agenda of the patient whose id is given.
     *
     * @param patientId Id of the patient whose Agenda needs to be returned.
     * @return Agenda of patient whose id given.
     * @throws InvalidIdException If given id does not exist.
     */
    public Agenda findAgendaByPatientId(String patientId) throws InvalidIdException {
        Agenda agenda = agendaRepository.findByPatientId(patientId);
        if (agenda != null) {
            return agenda;
        } else {
            throw new InvalidIdException();
        }
    }

    /**
     * This method removes an Event and the associated AssignedExercise from the
     * Agenda of the Patient whose id is given.
     *
     * @param patientId	Id of the patient in whose agenda the event needs to be
     * deleted.
     * @param assignedExerciseId Id of the assignedExercise that is coupled to
     * the event.
     * @return The Agenda where the event is deleted from
     * @throws InvalidIdException If there is no assignedExercise with given id.
     * @throws AccessDeniedException If the therapist is not assigned to this
     * patient.
     */
    public Agenda deleteEvent(String patientId, String assignedExerciseId) throws InvalidIdException, AccessDeniedException {
        Agenda agenda = findAgendaByPatientIdForTherapist(patientId);
        Event event = agenda.getEventByAssignedExerciseId(assignedExerciseId);
        assignedExerciseService.deleteAssignedExercise(assignedExerciseId);
        agenda.getEvents().remove(event);
        Agenda savedAgenda = agendaRepository.save(agenda);
        return savedAgenda;
    }

    /**
     * This method will find all events with the given date and return their
     * assignedExerciseIds.
     *
     * @param patientId If of the patient that you need the AssignedExerciseIds
     * from.
     * @param date The date that the AssignedExerciseIds must have.
     * @return An ArrayList of AssignedExerciseIds.
     */
    public ArrayList<String> findAssignedExerciseIdsByDate(String patientId, String date) {
        ArrayList<String> assignedExerciseIds;
        Agenda agenda = agendaRepository.findByPatientId(patientId);
        assignedExerciseIds = agenda.getAssignedExerciseIdByEventDate(date);
        if (assignedExerciseIds != null) {
            return assignedExerciseIds;
        } else {
            return null;
        }
    }

    /**
     * This method will add the given list of events to the agenda of the given
     * patientId and save it in the database.
     *
     * @param patientId Id of patient in whose agenda the events need to be
     * added.
     * @param sentEvents List of sentEvents that needs to be added to the
     * agenda.
     * @return The agenda that the event are saved in.
     * @throws InvalidIdException If id of patient is invalid.
     * @throws AccessDeniedException If current Therapist is not assigned to
     * this patient.
     */
    public Agenda addEvents(String patientId, List<SentEvent> sentEvents) throws InvalidIdException, AccessDeniedException {
        Agenda agenda = findAgendaByPatientIdForTherapist(patientId);
        for (SentEvent sentEvent : sentEvents) {
            AssignedExercise assignedExercise = assignedExerciseService.createAssignedExercise(sentEvent.getExerciseId(), sentEvent.getRepetitions(), patientId);
            String title = exerciseService.findExerciseById(sentEvent.getExerciseId()).getExerciseName() + " " + sentEvent.getRepetitions() + "x";
            Event event = new Event(assignedExercise.getId(), title, sentEvent.getStart(), sentEvent.getColor());
            agenda.addEvent(event);
        }
        Agenda savedAgenda = agendaRepository.save(agenda);
        return savedAgenda;
    }
}
