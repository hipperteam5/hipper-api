package services;

import Exceptions.InvalidIdException;
import Exceptions.AccessDeniedException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import models.Agenda;
import models.AssignedExercise;
import models.DailyAverageResult;
import models.Event;
import models.ResultsAgenda;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.AgendaRepository;
import repositories.AssignedExerciseRepository;

/**
 *
 * @author Maarten Petit
 */
@Service
public class ResultsAgendaService {

    @Autowired
    private AgendaRepository agendaRepositroy;

    @Autowired
    private AgendaService agendaService;

    @Autowired
    private AssignedExerciseRepository assignedExerciseRepository;

    @Autowired
    private PatientService patientService;
    
    Logger log = Logger.getLogger(ResultsAgendaService.class);

    public ResultsAgenda getResultsAgenda(String patientId) throws InvalidIdException, AccessDeniedException, ParseException {
        patientService.findByIdForTherapist(patientId);                                                             //Check if therapist is assigned to this patient & if patientId is valid.
        Agenda agenda = agendaRepositroy.findByPatientId(patientId);                                    //Get Agenda from given patient.
        List<Event> events = agenda.getEvents();                                                        //Get all events from the Agenda.
        ArrayList<DailyAverageResult> dailyAverageResults = new ArrayList();                            //Create new ArrayList with DailyAverageResults.
        for (Event event : events) {                                                                    //Loop through all the events.
            DailyAverageResult dailyAverageResult = new DailyAverageResult(event.getStart());           //Create new dailyAverageResult with event date.
            if (!dailyAverageResults.contains(dailyAverageResult) && checkCurrentDate(dailyAverageResult.getDate())) {
                dailyAverageResults.add(dailyAverageResult);                                            //Add dailyAverageResult with event date to dailyAverageResults.
            }
        }

        for (DailyAverageResult dailyAverageResult : dailyAverageResults) {                             //Loop through all the dailyAverageResults.
            double plannedAverage = 0;                                                                  //Number of repetitions planned on this day.
            double resultsAverage = 0;                                                                  //Nubmer of repetitions done by the patient on this day.
            ArrayList<String> assignedExerciseIds = agendaService.findAssignedExerciseIdsByDate(patientId, dailyAverageResult.getDate());
            for (String assignedExerciseId : assignedExerciseIds) {
                AssignedExercise assignedExercise = assignedExerciseRepository.findOne(assignedExerciseId);
                plannedAverage += assignedExercise.getRepetitions();                                    //Add the planned repetitions to the plannedAverage.
                resultsAverage += assignedExercise.getResults();                                        //Add the results to the resultsAverage.
            }
            double averageResult = (plannedAverage / resultsAverage);
            log.info("planned: " + plannedAverage + "results: " + resultsAverage );
            log.info(averageResult);
            if (resultsAverage == 0) {                                  //If there are no results: set color to red(== 3).
                dailyAverageResult.setResultColor(3);
            } else if (averageResult >= 1 && averageResult < 1.33) {    //If averageResults is between 1(100%) & 1.33(>75%): set color to green(== 1)
                dailyAverageResult.setResultColor(1);
            } else if (averageResult >= 1.33 && averageResult < 2) {    //If averageResults is between 1.33(<75%) & 2(>50%): set color to yellow(== 2)
                dailyAverageResult.setResultColor(2);
            } else if (averageResult >= 2) {                            //If averageResults is greater than 2(<50%): set color to red(== 3)
                dailyAverageResult.setResultColor(3);
            }
        }

        ResultsAgenda resultsAgenda = new ResultsAgenda(patientId, dailyAverageResults);
        return resultsAgenda;
    }

    public boolean checkCurrentDate(String date) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date givenDate = dateFormat.parse(date);
        Date currentDate = new Date();
        return givenDate.before(currentDate);
    }

}
