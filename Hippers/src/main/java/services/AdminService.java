/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import Exceptions.InvalidIdException;
import Exceptions.UsernameUnavailableException;
import com.mongodb.DuplicateKeyException;
import configuration.UserConstants;

import models.Admin;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.AdminRepository;

/**
 *
 * @author mats
 */
@Service
public class AdminService {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private HashService hashService;

    private Logger log = Logger.getLogger(AdminService.class);

    public Admin createAdmin(String username, String password) throws UsernameUnavailableException {
        if (usernameAvailable(username)) {
            Admin sentAdmin = new Admin(username);

            try {
                Admin savedAdmin = adminRepository.save(sentAdmin);
                hashService.createHash(UserConstants.ADMIN_ROLE, savedAdmin.getId(), password);
                return savedAdmin;
            } catch (DuplicateKeyException e) {
                log.info(e.getMessage());
                throw new UsernameUnavailableException();
            }
        } else {
            throw new UsernameUnavailableException();
        }
    }

    public boolean usernameAvailable(String username) {
        if (adminRepository.findByUsername(username) != null) {
            return false;
        }
        return true;
    }

    public Admin findAdminById(String id) throws InvalidIdException {
        Admin admin = adminRepository.findById(id);
        if (admin != null) {
            return admin;
        } else {
            throw new InvalidIdException();
        }
    }

    /*
     @param id of the admin that needs to be deleted
     @return the id of the deleted admin (if de id is valid)
     @throws InvalidIdException to check if the id is valid
     */
    public String deleteAdmin(String id) throws InvalidIdException {
        if (adminRepository.findById(id) != null) {
            adminRepository.delete(id);
            hashService.deleteAdminHash(id);
            return id;
        } else {
            throw new InvalidIdException();
        }
    }
}
