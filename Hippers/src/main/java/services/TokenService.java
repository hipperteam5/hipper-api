/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.auth0.jwt.Algorithm;
import com.auth0.jwt.ClaimSet;
import configuration.AppConstants;
import configuration.Authorization.JWT;
import configuration.Authorization.JwtPayloadHandler;
import models.Admin;
import models.JwtAdminTokenResult;
import models.JwtPatientTokenResult;
import models.JwtTherapistTokenResult;
import models.JwtTokenResult;
import models.Patient;
import models.Therapist;
import models.Token;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author yorin
 */
@Service
public class TokenService {

    Logger log = Logger.getLogger(TokenService.class);

    public JwtTokenResult createTherapistToken(Therapist therapist) throws Exception {
        Token token = new Token(therapist.getId(), therapist.getUsername(), therapist.getRoles());

        //Generate hipperaccesstoken
        String encoded = createToken(token);

        //Build Result
        JwtTherapistTokenResult encodedToken = new JwtTherapistTokenResult(encoded, therapist);

        return encodedToken;
    }

    public JwtTokenResult createAdminToken(Admin admin) throws Exception {

        Token token = new Token(admin.getId(), admin.getUsername(), admin.getRoles());

        //Generate hipperaccesstoken
        String encoded = createToken(token);

        //Generate Result
        JwtAdminTokenResult result = new JwtAdminTokenResult(encoded, admin);

        return result;
    }
    
    public JwtTokenResult createPatientToken(Patient patient) throws Exception {
        Token token = new Token(patient.getId(), patient.getPatientNr(), patient.getRoles());
        
        //Generate hipperaccesstoken
        String encoded = createToken(token);
        
        JwtPatientTokenResult result = new JwtPatientTokenResult(encoded, patient);
        return result;
    }

    private String createToken(Token token) throws Exception {
        //Set up needs for encoding
        JWT jwt = new JWT();
        // Sloppy way of setting payloadhandler, fix..
        jwt.setPayloadHandler(new JwtPayloadHandler());

        ClaimSet claimset = new ClaimSet();
        claimset.setExp(30000); //30000?

        // Encode token
        String encoded = jwt.encode(Algorithm.HS256, token, AppConstants.SECRET, claimset);
        log.info(encoded);

        return encoded;
    }

}
