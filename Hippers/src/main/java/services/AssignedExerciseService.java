/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import Exceptions.InvalidIdException;
import models.AssignedExercise;
import models.Exercise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.AssignedExerciseRepository;

/**
 *
 * @author Maarten Petit
 */
@Service
public class AssignedExerciseService {

    @Autowired
    private AssignedExerciseRepository assignedExerciseRepository;

    @Autowired
    private ExerciseService exerciseService;

    /**
     * This method creates a assignedExercise and stores it in the database.
     *
     * @param exerciseId Id of the exercise that needs to be assigned to the
     * patient.
     * @param repetitions Assigned number of repetitions for this exercise, if
     * value = 0 the method will set the number repetions to the dafault number
     * of repetitions for the exercise.
     * @param patientId Id of the patient that the exercise needs to be assigned
     * to.
     * @return The assignedExercise that is saved into the database.
     * @throws InvalidIdException If no exercise with given exercise id exitst.
     */
    public AssignedExercise createAssignedExercise(String exerciseId, int repetitions, String patientId) throws InvalidIdException {
        Exercise exercise = exerciseService.findExerciseById(exerciseId);

        if (repetitions == 0) {
            repetitions = exercise.getRepetitions();
        }

        AssignedExercise assignedExercise = new AssignedExercise(exercise.getId(), patientId, repetitions);
        AssignedExercise savedAssignedExercise = assignedExerciseRepository.save(assignedExercise);
        return savedAssignedExercise;
    }

    /**
     * This method will search for an AssignedExercise in the database with the
     * given id.
     *
     * @param assignedExerciseId The id of the assignedExercise that needs to be
     * returned.
     * @return The AssignedExercise with the given id.
     * @throws InvalidIdException If there is no AssignedExercise stored in the
     * database with the given id.
     */
    public AssignedExercise findAssignedExerciseById(String assignedExerciseId) throws InvalidIdException {
        AssignedExercise assignedExercise = assignedExerciseRepository.findOne(assignedExerciseId);
        if (assignedExercise != null) {
            return assignedExercise;
        } else {
            throw new InvalidIdException();
        }
    }

    /**
     * This method will remove the AssignedExercise with the given id from the
     * database.
     *
     * @param assignedExerciseId The id of the AssignedExercise that needs to be
     * deleted.
     * @throws InvalidIdException If there is no AssignedExercise stored in the
     * database with the given id.
     */
    public void deleteAssignedExercise(String assignedExerciseId) throws InvalidIdException {
        assignedExerciseRepository.delete(findAssignedExerciseById(assignedExerciseId));
    }

    public AssignedExercise getAssignedExerciseById(String AEid) throws InvalidIdException {
        AssignedExercise ae = assignedExerciseRepository.findOne(AEid);
        if (ae != null) {
            return ae;
        } else {
            throw new InvalidIdException();
        }
    }

}
