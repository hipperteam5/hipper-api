package services;

import Exceptions.InvalidIdException;
import Exceptions.InvalidWeeknumberException;
import Exceptions.NoResultsException;
import Exceptions.AccessDeniedException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import models.AssignedExercise;
import models.WeekGraphData;
import models.WeeklyAverageResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.AssignedExerciseRepository;
import repositories.ExerciseRepository;

/**
 *
 * @author Maarten Petit
 */
@Service
public class WeekGraphDataService {

    @Autowired
    private AgendaService agendaService;

    @Autowired
    private AssignedExerciseRepository assignedExerciseRepository;

    @Autowired
    private PatientService patientService;

    @Autowired
    private ExerciseRepository exerciseRepository;

    /**
     * This method will return the data that is needed for week graphs.
     *
     * @param patientId Id of patient that you need the resutls from.
     * @param year Year of the given week.
     * @param weeknumber Number of the week you want the results from.
     * @return The data that is used for the week graphs.
     * @throws InvalidWeeknumberException If weeknumber is invalid.
     * @throws InvalidIdException If patientId doesn't exitst.
     * @throws AccessDeniedException If therapist is not assigned to this
     * patient.
     * @throws NoResultsException If there are no results of the given week.
     */
    public WeekGraphData getWeekGraphData(String patientId, int year, int weeknumber) throws InvalidWeeknumberException, InvalidIdException, AccessDeniedException, NoResultsException {
        patientService.findByIdForTherapist(patientId);                         //Check if therapist is assigned to this patient & if patientId is valid.
        String[] datesInWeek = getDatesInWeek(year, weeknumber);                    //Get Array of strings with all the dates of the given week.
        WeekGraphData weekGraphData = new WeekGraphData(patientId, weeknumber);     //Create new WeekGraphData with given patientId & weeknumber.
        ArrayList<WeeklyAverageResults> weeklyAverageResults = new ArrayList<>(0);   //Create new ArrayList of WeeklyAverageResults.

        for (int i = 0; i < 7; i++) {                                                                                           //Loop through week.
            ArrayList<String> assignedExerciseIds = agendaService.findAssignedExerciseIdsByDate(patientId, datesInWeek[i]);     //Get all AassignedExerciseIds with current date.                                                                                                                
            for (String assignedExerciseId : assignedExerciseIds) {                                                             //Loop through all AssignedExercises.
                AssignedExercise assignedExercise = assignedExerciseRepository.findOne(assignedExerciseId); //Find the AssignedExercise with given date.
                int indexExerciseResultsArray = getIndexByExerciseId(weeklyAverageResults, assignedExercise.getExerciseId());   //Get index of Exercise is weeklyAverageResults.
                if (indexExerciseResultsArray == -1) {                                                                          //Check if there already is a  WeeklyAverageResults with this exerciseId.
                    WeeklyAverageResults weeklyAverageResult = new WeeklyAverageResults(assignedExercise.getExerciseId());      //Create new WeekylAverageResults with ExerciseId.
                    weeklyAverageResult.setExerciseName(exerciseRepository.findOne(assignedExercise.getExerciseId()).getExerciseName());    //Set name to name of given exerciseId.
                    weeklyAverageResults.add(weeklyAverageResult);                                                              //Add the WeekylAverageResults in ArrayList if not found.
                    indexExerciseResultsArray = weeklyAverageResults.size() - 1;
                }
                WeeklyAverageResults tempWeeklyAverageResults = weeklyAverageResults.get(indexExerciseResultsArray);    //Get WeeklyAverageResults from ArrayList with results.
                int assignedExerciseRepetitions = assignedExercise.getRepetitions();                                    //Get repetitions from AssignedExercise.
                int assignedExerciseResults = assignedExercise.getResults();                                            //Get results from AssignedExercise.
                tempWeeklyAverageResults.addToPlanning(i, assignedExerciseRepetitions);                                 //Add repetitions to WeeklyAverageResults.
                tempWeeklyAverageResults.addToResults(i, assignedExerciseResults);                                      //Add results to WeekylAverageResults.
                if (tempWeeklyAverageResults.isTargetAchieved()) {                                                      //If target is not achieved do nothing
                    if (assignedExerciseRepetitions - assignedExerciseResults > 0) {                                    //If target is achieved check if current AssignedExercise is achieved.                    
                        tempWeeklyAverageResults.setTargetAchieved(false);                                              //If target is not achieved set to false.
                    }
                }
                weeklyAverageResults.set(indexExerciseResultsArray, tempWeeklyAverageResults);                          //Add to WeeklyAverageResults ArrayList.
            }
        }

        if (!weeklyAverageResults.isEmpty()) {              //If results is empty throw NoResultsException.    
            weekGraphData.setResults(weeklyAverageResults); //Set results to ArrayList with all WeeklyAverageResults.  
            return weekGraphData;
        } else {
            throw new NoResultsException();
        }
    }

    public int getIndexByExerciseId(ArrayList<WeeklyAverageResults> weeklyAverageResult, String exerciseId) {
        for (int i = 0; i < weeklyAverageResult.size(); i++) {
            if (weeklyAverageResult.get(i).getExerciseId().equals(exerciseId)) {
                return i;
            }
        }
        return -1;  //If the list not contains a WeeklyAverageResults with the given ExerciseId.
    }

    /**
     * This method will give you all the dates of the given weeknubmer.
     *
     * @param year Year of the week you want the dates from.
     * @param weeknumber Number of the week that you want the dates from.
     * @return Array with string that represent all the days in the given week
     * (yyyy-MM-dd). Returns null if given weeknumber is incorrect.
     * @throws InvalidWeeknumberException If given weeknumber is invalid (<0 or
     * >52)
     */
    public String[] getDatesInWeek(int year, int weeknumber) throws InvalidWeeknumberException {
        if (weeknumber > 0 && weeknumber <= 52) {
            String[] daysInWeek = new String[7];
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.WEEK_OF_YEAR, weeknumber);
            cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            daysInWeek[0] = sdf.format(cal.getTime());
            cal.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
            daysInWeek[1] = sdf.format(cal.getTime());
            cal.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
            daysInWeek[2] = sdf.format(cal.getTime());
            cal.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
            daysInWeek[3] = sdf.format(cal.getTime());
            cal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
            daysInWeek[4] = sdf.format(cal.getTime());
            cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
            daysInWeek[5] = sdf.format(cal.getTime());
            cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
            daysInWeek[6] = sdf.format(cal.getTime());
            return daysInWeek;
        } else {
            throw new InvalidWeeknumberException();
        }
    }
}
