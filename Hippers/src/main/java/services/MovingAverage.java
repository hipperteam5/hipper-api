/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author yorin
 */
public class MovingAverage {

    private double avg;
    private double total;
    int windowSize;
    Queue<Double> window = new LinkedList<Double>();

    public MovingAverage(int windowSize) {
        this.windowSize = windowSize;
    }

    public double getAvg() {
//        System.out.println("AVG: " + avg);
        return avg;
    }

    public void addVal(double val) {
        if (window.size() > windowSize) {
            total -= window.remove();
        }
        total += val;
        window.add(val);
        if (window.size() > 0 ) {
            avg = total / window.size();
        }
    }
    
    public void clear() {
        avg = 0;
        total = 0;
        window = new LinkedList<Double>();
    }

}
