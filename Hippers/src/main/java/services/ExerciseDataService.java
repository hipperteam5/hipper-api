/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import Exceptions.InvalidIdException;
import models.ExerciseData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.ExerciseDataRepository;

/**
 *
 * @author yorin
 */
@Service
public class ExerciseDataService {

    Logger log = Logger.getLogger(ExerciseData.class);
    private int windowSize = 10;

    MovingAverage movingAverage = new MovingAverage(windowSize);

    @Autowired
    private ExerciseDataRepository exerciseDataRepository;

    public int countReps(String id) throws InvalidIdException {
        int xPeaks;
        int yPeaks;
        int zPeaks;
        ExerciseData exerciseData = null;
        try {
            exerciseData = exerciseDataRepository.findById(id);
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        if (exerciseData != null) {
            double[] x = getMovingAverage(exerciseData.getX());
            double[] y = getMovingAverage(exerciseData.getY());
            double[] z = getMovingAverage(exerciseData.getZ());
            long[] timestamps = exerciseData.getTimestamp();
            int runId = exerciseData.getRunId();

            xPeaks = count(x);
            yPeaks = count(y);
            zPeaks = count(z);

            if (xPeaks == yPeaks || xPeaks == zPeaks) {
                return xPeaks;
            } else if (yPeaks == zPeaks) {
                return yPeaks;
            } else {
                return (xPeaks + yPeaks + zPeaks) / 3;
            }
        } else {
            throw new InvalidIdException();
        }

    }

    private int count(double[] vals) {
        int result;
        PeakDetector PeakDetector = new PeakDetector(vals, windowSize, 0.3);
        result = PeakDetector.findPeaks();
        return result;
    }

    private double[] getMovingAverage(double[] vals) {
        int length = vals.length / windowSize;
        double[] result = new double[length];
        for (int i = 0, j = 0; i < vals.length; i++) {
            movingAverage.addVal(vals[i]);

            if (i % windowSize == 0) {
                result[j] = movingAverage.getAvg();
                j++;
                if (j >= length) {
                    break;
                }
            }
        }
        return result;
    }

    /**
     * This method will return the results of the given AssignedExercise. The
     * method returns the amount of peaks that the algoritem will detect from
     * the raw data.
     *
     * @param assignedExerciseId Id of the AssignedExercise you want the results
     * from
     * @return Results of the given AssignedExercise(number of peaks).
     * @throws InvalidIdException
     */
    public int getResults(String assignedExerciseId) throws InvalidIdException {
        ExerciseData exerciseData = exerciseDataRepository.findByAssignedExerciseId(assignedExerciseId);
        if (exerciseData != null) {
            int peaks = countReps(exerciseData.getId());
            return peaks;
        }
        return 0;
    }

}
