/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import Exceptions.InvalidCredentialsException;
import Exceptions.InvalidIdException;
import configuration.UserConstants;
import models.Admin;
import models.Therapist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.AdminRepository;
import repositories.TherapistRepository;

/**
 *
 * @author mats
 */
@Service
public class AuthenticationService {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private TherapistRepository therapistRepository;

    @Autowired
    private HashService hashServ;

    public Therapist authenticateTherapist(String username, String password) throws InvalidCredentialsException, InvalidIdException {

        Therapist therapist = therapistRepository.findByUsername(username);

        //If User found
        if (therapist != null) {

            //Check password
            if (hashServ.matchPassword(UserConstants.THERAPIST_ROLE, therapist.getId(), password)) {
                // If password matches..
                return therapist;
            }
        }
        //If Invalid Credentials..
        throw new InvalidCredentialsException();
    }

    public Admin authenticateAdmin(String username, String password) throws InvalidCredentialsException, InvalidIdException {

        Admin admin = adminRepository.findByUsername(username);

        //If User found
        if (admin != null) {

            //Check password
            if (hashServ.matchPassword(UserConstants.ADMIN_ROLE, admin.getId(), password)) {

                // If password matches..
                return admin;
            }
        }
        //If Invalid Credentials..
        throw new InvalidCredentialsException();
    }

}
