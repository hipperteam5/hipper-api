/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import Exceptions.InvalidIdException;
import Exceptions.UsernameUnavailableException;
import configuration.UserConstants;
import models.Therapist;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import repositories.TherapistRepository;

/**
 *
 * @author yorin
 */
@Service
public class TherapistService {

    @Autowired
    TherapistRepository therapistRepository;

    @Autowired
    HashService hashService;

    Logger log = Logger.getLogger(TherapistService.class);

    public Therapist createTherapist(String username, String password, String firstName, String lastName,
            String phoneNr, String therapistType) throws UsernameUnavailableException {
        if (usernameAvailable(username)) {
            Therapist sentTherapist = new Therapist(username, firstName, lastName, phoneNr, therapistType);
            try {
                log.info("SAVING PATIENT");
                log.info(sentTherapist);
                Therapist savedTherapist = therapistRepository.save(sentTherapist);
                log.info("SAVING HASH");
                hashService.createHash(UserConstants.THERAPIST_ROLE, savedTherapist.getId(), password);
                return savedTherapist;
            } catch (DuplicateKeyException e) {
                log.info(e.getMessage());
                throw new DuplicateKeyException(e.getMessage(), e);
            }
        } else {
            throw new UsernameUnavailableException();
        }

    }

    public boolean usernameAvailable(String username) {
        if (therapistRepository.findByUsername(username) != null) {
            return false;
        }
        return true;
    }

    public Therapist findTherapistById(String id) throws InvalidIdException {
        Therapist therapist = therapistRepository.findById(id);
        if (therapist != null) {
            return therapist;
        } else {
            throw new InvalidIdException();
        }
    }

    /*
     @param id of the therapist that needs to be delted
     @retrun id of the therapist that is deleted (only if id is valid)
     @throws InvalidIdException if Id does not exists
     */
    public String deleteTherapist(String id) throws InvalidIdException {
        if (therapistRepository.findById(id) != null) {
            therapistRepository.delete(id);
            hashService.deleteTherapistHash(id);
            return id;
        } else {
            throw new InvalidIdException();
        }
    }

    public Therapist updateTherapist(Therapist therapist, String id) throws InvalidIdException {
        if (therapistRepository.findById(therapist.getId()) != null) {
            Therapist updatedTherapist = therapistRepository.save(therapist);
            return updatedTherapist;
        }
        throw new InvalidIdException();
    }
}
