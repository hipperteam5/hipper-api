/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import Exceptions.InvalidIdException;
import Exceptions.AccessDeniedException;
import java.util.List;
import models.Patient;
import models.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import repositories.PatientRepository;

/**
 *
 * @author yorin
 */
@Service
public class PatientService {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private AgendaService agendaService;

    @Autowired
    private TherapistService therapistService;

    /**
     * This method will create a patient and store it in the database.
     *
     * @param sentPatient The patient object that needs to be saved into the
     * database.
     * @return The patient that is saved into the database.
     */
    public Patient createPatient(Patient sentPatient) {
        Token activeUser = (Token) SecurityContextHolder.getContext().getAuthentication().getDetails();
        sentPatient.getTherapistId().add(activeUser.getUser_id());
        Patient createdPatient = patientRepository.save(sentPatient);
        agendaService.createAgenda(createdPatient.getId());
        return createdPatient;
    }

    /**
     * This method will return all the patients of the given therapist.
     *
     * @return List of patient object that the therapist is assigned to.
     * @throws InvalidIdException If no therapist with given id exists.
     */
    public List<Patient> findPatientsByTherapist() throws InvalidIdException {
        Token activeUser = (Token) SecurityContextHolder.getContext().getAuthentication().getDetails();
        therapistService.findTherapistById(activeUser.getUser_id());
        List<Patient> patients = patientRepository.findByTherapistId(activeUser.getUser_id());
        return patients;
    }

    /**
     * This method will search for the patient with the given id. This specific
     * method is used only when the agenda is needed for a user of the
     * webservice.
     *
     * @param id Id of the patient that needs to be returned.
     * @return The patient with the given Id.
     * @throws InvalidIdException If no patient with given id exists.
     * @throws AccessDeniedException If the therapist is no assigned to given
     * patient.
     */
    public Patient findByIdForTherapist(String id) throws InvalidIdException, AccessDeniedException {
        Patient patient = findById(id);
        Token activeUser = (Token) SecurityContextHolder.getContext().getAuthentication().getDetails();
        if (patient.getTherapistId().contains(activeUser.getUser_id())) {
            return patient;
        } else {
            throw new AccessDeniedException();
        }
    }

    public Patient findById(String id) throws InvalidIdException {
        Patient patient = patientRepository.findById(id);
        if (patient != null) {
            return patient;
        } else {
            throw new InvalidIdException();
        }
    }

    //update patient
    public Patient updatePatient(Patient patient, String id) throws InvalidIdException {
        if (patientRepository.findById(id) != null) {
            patient.setId(id);
            Patient updatedPatient = patientRepository.save(patient);
            return updatedPatient;
        }
        throw new InvalidIdException();
    }

    public void addTherapistToPatient() {
        throw new UnsupportedOperationException("Adding a Therapist to a patient is not yet supported");
    }

    /**
     * This method checks if the current user is authorized to work with the
     * given patient.
     *
     * @param patientId The id of the patient that is used.
     * @throws AccessDeniedException If current user is not authorized to work
     * with this patient.
     */
    public void checkAuthorization(String patientId) throws AccessDeniedException {
        Patient patient = patientRepository.findById(patientId);
        Token currentActiveUser = (Token) SecurityContextHolder.getContext().getAuthentication().getDetails();
        if (!patient.getTherapistId().contains(currentActiveUser.getUser_id())) {
            throw new AccessDeniedException();
        }
    }

}
