/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

/**
 *
 * @author yorin
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 *
 * @author yorin
 */
public class FileManager {

    private File file;
    private int rows = 0;
    private BufferedReader br;
    private String nextLine = "";
    private String splitBy = ",";
    private String[] val = new String[6];
    private double[] x;
    private double[] y;
    private double[] z;
    private long[] timeStamp;
    private int runId = 0;

    public FileManager(String fileName) {
        this.file = new File(fileName);
    }

    private void countRows() {
        try {
            br = new BufferedReader(new FileReader(file));
            int i = 0;
            while ((nextLine = br.readLine()) != null) {
                rows++;
            }
            rows--;
//            System.out.println("ROWS: " + rows);
            x = new double[rows];
            timeStamp = new long[rows];
            y = new double[rows];
            z = new double[rows];

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void read() {
        countRows();
        try {
            br = new BufferedReader(new FileReader(file));
            int i = 0;
            nextLine = br.readLine();
            while ((nextLine = br.readLine()) != null) {
                val = nextLine.split(splitBy);
                x[i] = Double.parseDouble(val[0]);
                y[i] = Double.parseDouble(val[1]);
                z[i] = Double.parseDouble(val[2]);
                timeStamp[i] = Long.parseLong(val[3]);
                if (runId == 0) {
                    runId = Integer.parseInt(val[4]);
                }
                i++;
            }
            br.close();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public double[] getX() {
        return x;
    }

    public double[] getY() {
        return y;
    }

    public double[] getZ() {
        return z;
    }

    public long[] getTimeStamp() {
        return timeStamp;
    }

    public long checkFile() {
        return file.length();
    }
    
    public int getRunId() {
        return runId;
    }
    

    public void check() {
        System.out.println(file.getAbsolutePath());
        System.out.println(file.isFile());
        System.out.println(file.length());
        System.out.println(rows);
    }
}
