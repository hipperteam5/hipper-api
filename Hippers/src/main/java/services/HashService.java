/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import Exceptions.InvalidCredentialsException;
import Exceptions.InvalidIdException;
import configuration.UserConstants;
import models.Hash;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import repositories.HashRepository;

/**
 *
 * @author Yorin
 */
@Service
public class HashService {

    @Autowired
    private HashRepository hashRepository;

    private BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
    private Logger log = Logger.getLogger(HashService.class);

    public Hash createHash(int type, String id, String password) {
        Hash hash = null;
        String encoded = bcrypt.encode(password);
        switch (type) {
            case UserConstants.ADMIN_ROLE:
                hash = new Hash(id, null, null, encoded);
                break;
            case UserConstants.PATIENT_ROLE:
                hash = new Hash(null, null, id, encoded);
                break;
            case UserConstants.THERAPIST_ROLE:
                hash = new Hash(null, id, null, encoded);
                break;
        }
        try {
            hashRepository.save(hash);
            return hash;
        } catch (DuplicateKeyException ex) {
            throw new DuplicateKeyException(ex.getMessage(), ex);
        }
    }

    public boolean matchPassword(int type, String id, String password) throws InvalidIdException, InvalidCredentialsException {
        Hash hash = null;
        log.info(id);
        try {
            switch (type) {
                case UserConstants.ADMIN_ROLE:
                    hash = hashRepository.findByAdminId(id);
                    break;
                case UserConstants.PATIENT_ROLE:
                    hash = hashRepository.findByPatientId(id);
                    break;
                case UserConstants.THERAPIST_ROLE:
                    hash = hashRepository.findByTherapistId(id);
                    break;
            }
        } catch (Exception e) {
            log.info("Can't find Hash");
            throw new InvalidCredentialsException();

        }
        log.info("HASH FOUND");
        log.info(hash);
        if (hash != null) {
            log.info("hash not null");
            if (bcrypt.matches(password, hash.getHash())) {
                log.info("password match");
                return true;
            } else {
                log.info("password mismatch");
                return false;
            }
        } else {
            throw new InvalidIdException();
        }
    }

    /*
     @param the hash id that needs to be deleted
     @return the id of the deleted hash (if id is valid)
     @throws InvalidIdException if Id does not exists
     */
    public void deleteTherapistHash(String id) {
        String hashId = hashRepository.findByTherapistId(id).getId();
        if (hashRepository.findOne(hashId) != null) {
            hashRepository.delete(hashId);
        } else {
            log.info("Hash doesn't exist!");
        }
    }

    public void deleteAdminHash(String id) {
        String hashId = hashRepository.findByAdminId(id).getId();
        if (hashRepository.findOne(hashId) != null) {
            hashRepository.delete(hashId);
        } else {
            log.info("Hash doesn't exist!");
        }
    }
}
