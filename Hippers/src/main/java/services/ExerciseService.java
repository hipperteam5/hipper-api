/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import Exceptions.InvalidIdException;
import models.Exercise;
import models.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import repositories.ExerciseRepository;

/**
 *
 * @author Maarten
 */
@Service
public class ExerciseService {

    @Autowired
    private ExerciseRepository exerciseRepository;

    /**
     * This method will return the exercise with given id.
     *
     * @param id Id of the exercise that needs to be returned.
     * @return The exercise with the given id.
     * @throws InvalidIdException If no exercise with given id exists.
     */
    public Exercise findExerciseById(String id) throws InvalidIdException {
        Exercise exercise = exerciseRepository.findOne(id);
        if (exercise != null) {
            return exercise;
        } else {
            throw new InvalidIdException();
        }
    }

    /**
     * This method will create an exercise and store it in the database.
     *
     * @param sentExercise The sentExercise object which contains the details
     * for the exercise that needs to be created.
     * @return The exercise that is saved into the database.
     */
    public Exercise createExercise(Exercise sentExercise) {
        Token activeUser = (Token) SecurityContextHolder.getContext().getAuthentication().getDetails();
        sentExercise.setTherapistId(activeUser.getUser_id());
        Exercise createdExercise = exerciseRepository.save(sentExercise);
        return createdExercise;
    }

    /**
     * This method will delete the exercise with the given id from the database.
     *
     * @param id Id of the exercise that needs to be deleted.
     * @throws InvalidIdException If no exercise with given id exists.
     */
    public void deleteExercise(String id) throws InvalidIdException {
        findExerciseById(id);
        exerciseRepository.delete(id);
    }

    /**
     * This method will update the exercise with given id.
     *
     * @param sentExercise The sentExercise object which contains the details
     * for the exercise that needs to be updated.
     * @param id Id of the exercise that needs to be updated.
     * @return The updated exercise that is saved into the database.
     * @throws InvalidIdException If no exercise with given id exists.
     */
    public Exercise updateExercise(Exercise sentExercise, String id) throws InvalidIdException {
        findExerciseById(id);
        sentExercise.setId(id);
        Exercise updatedExercise = exerciseRepository.save(sentExercise);
        return updatedExercise;
    }
}
