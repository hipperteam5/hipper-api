package controllers;

import configuration.ErrorConstants;
import Exceptions.ErrorResource;
import Exceptions.InvalidCredentialsException;
import Exceptions.InvalidIdException;
import Exceptions.InvalidJsonException;
import Exceptions.InvalidTokenException;
import Exceptions.InvalidWeeknumberException;
import Exceptions.NoResultsException;
import Exceptions.AccessDeniedException;

import Exceptions.UsernameUnavailableException;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.json.simple.parser.JSONParser;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 *
 * @author mats
 */
@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

    Logger logger = Logger.getLogger(ExceptionController.class);
    JSONParser parser = new JSONParser();

    @ExceptionHandler({DuplicateKeyException.class})
    protected ResponseEntity<Object> handleDuplicateKey(RuntimeException e, WebRequest request) {
        logger.log(Priority.INFO, "Duplicate Key Exception");

        ErrorResource error = new ErrorResource();
        error.setMessage(ErrorConstants.DUP_KEY_MESSAGE);
        error.setCode(ErrorConstants.DUP_KEY_CODE);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return handleExceptionInternal(e, error, headers, HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler({UsernameUnavailableException.class})
    protected ResponseEntity<Object> handleUnavailableUsername(UsernameUnavailableException e, WebRequest request) {
        logger.log(Priority.INFO, "Unavailable username");

        ErrorResource error = new ErrorResource();
        error.setMessage(e.getMessage());
        error.setCode(e.getCode());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return handleExceptionInternal(e, error, headers, HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler({InvalidCredentialsException.class})
    protected ResponseEntity<Object> handleInvalidCredentials(InvalidCredentialsException e, WebRequest request) {
        logger.log(Priority.INFO, "Invalid Credentials");

        ErrorResource error = new ErrorResource();
        error.setMessage(e.getMessage());
        error.setCode(e.getCode());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return handleExceptionInternal(e, error, headers, HttpStatus.UNAUTHORIZED, request);
    }

    @ExceptionHandler({AccessDeniedException.class})
    protected ResponseEntity<Object> handleToegangGeweigerd(AccessDeniedException e, WebRequest request) {
        logger.log(Priority.INFO, "Toegang Geweigerd");

        ErrorResource error = new ErrorResource();
        error.setMessage(e.getMessage());
        error.setCode(e.getCode());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpStatus status = HttpStatus.FORBIDDEN;

        return handleExceptionInternal(e, error, headers, status, request);
    }

    @ExceptionHandler({InvalidTokenException.class})
    protected ResponseEntity<Object> handleInvalidToken(InvalidTokenException e, WebRequest request) {
        logger.log(Priority.INFO, "Invalid Token");

        ErrorResource error = new ErrorResource();
        error.setMessage(e.getMessage());
        error.setCode(e.getCode());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpStatus status = HttpStatus.UNAUTHORIZED;

        return handleExceptionInternal(e, error, headers, status, request);
    }

    @ExceptionHandler({InvalidIdException.class})
    protected ResponseEntity<Object> handleInvalidIdException(InvalidIdException e, WebRequest request) {
        logger.log(Priority.INFO, "Invalid Id");

        ErrorResource error = new ErrorResource();
        error.setMessage(e.getMessage());
        error.setCode(e.getCode());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpStatus status = HttpStatus.BAD_REQUEST;

        return handleExceptionInternal(e, error, headers, status, request);
    }

    @ExceptionHandler({InvalidJsonException.class})
    protected ResponseEntity<Object> handleInvalJsonException(InvalidJsonException e, WebRequest request) {
        logger.info("Invalid JSON");

        ErrorResource error = new ErrorResource();
        error.setMessage(e.getMessage());
        error.setCode(e.getCode());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpStatus status = HttpStatus.BAD_REQUEST;

        return handleExceptionInternal(e, error, headers, status, request);
    }

    @ExceptionHandler({InvalidWeeknumberException.class})
    protected ResponseEntity<Object> handleInvalidWeeknumberException(InvalidWeeknumberException e, WebRequest request) {

        ErrorResource error = new ErrorResource();
        error.setMessage(e.getMessage());
        error.setCode(e.getCode());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpStatus status = HttpStatus.BAD_REQUEST;

        return handleExceptionInternal(e, error, headers, status, request);
    }

    @ExceptionHandler({NoResultsException.class})
    protected ResponseEntity<Object> handleNoResultsException(NoResultsException e, WebRequest request) {

        ErrorResource error = new ErrorResource();
        error.setMessage(e.getMessage());
        error.setCode(e.getCode());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpStatus status = HttpStatus.BAD_REQUEST;

        return handleExceptionInternal(e, error, headers, status, request);
    }
}
