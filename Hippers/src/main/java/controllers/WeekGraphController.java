package controllers;

import Exceptions.InvalidIdException;
import Exceptions.InvalidWeeknumberException;
import Exceptions.NoResultsException;
import Exceptions.AccessDeniedException;
import Routes.WeekGraphRoutes;
import models.WeekGraphData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import services.WeekGraphDataService;

/**
 *
 * @author Maarten Petit
 */
@Controller
public class WeekGraphController {

    @Autowired
    private WeekGraphDataService weekGraphDataService;

    @PreAuthorize("hasRole('THERAPIST')")
    @RequestMapping(value = WeekGraphRoutes.BY_WEEKNUMBER, method = RequestMethod.GET)
    public @ResponseBody
    WeekGraphData getWeekGraphData(@PathVariable String patientId, @PathVariable("year") int year, @PathVariable("weeknumber") int weeknumber) throws InvalidIdException, AccessDeniedException, InvalidWeeknumberException, NoResultsException {
        WeekGraphData weekGraphData = weekGraphDataService.getWeekGraphData(patientId, year, weeknumber);
        return weekGraphData;
    }

}
