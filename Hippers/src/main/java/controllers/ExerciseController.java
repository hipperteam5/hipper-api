/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Exceptions.InvalidIdException;
import Routes.ExerciseRoutes;
import java.util.List;
import models.Exercise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import repositories.ExerciseRepository;
import services.ExerciseService;

/**
 *
 * @author Maarten
 */
@Controller
public class ExerciseController {

    @Autowired
    ExerciseService exerciseService;

    @Autowired
    ExerciseRepository exerciseRepository;

    @PreAuthorize("hasRole('THERAPIST')")
    @RequestMapping(value = ExerciseRoutes.CREATE, method = RequestMethod.POST)
    public @ResponseBody
    Object createExercise(@RequestBody Exercise sentExercise) {
        Exercise createdExercise = exerciseService.createExercise(sentExercise);
        return createdExercise;
    }

    @PreAuthorize("hasRole('THERAPIST')")
    @RequestMapping(value = ExerciseRoutes.BASE, method = RequestMethod.GET)
    public @ResponseBody
    List<Exercise> allExercise() {
        List<Exercise> exercises = exerciseRepository.findAll();
        return exercises;
    }

    @PreAuthorize("hasRole('THERAPIST')")
    @RequestMapping(value = ExerciseRoutes.BY_ID, method = RequestMethod.GET)
    public @ResponseBody
    Exercise findById(@PathVariable String id) throws InvalidIdException {
        Exercise exercise = exerciseService.findExerciseById(id);
        return exercise;
    }

    @PreAuthorize("hasRole('THERAPIST')")
    @RequestMapping(value = ExerciseRoutes.DELETE, method = RequestMethod.DELETE)
    public @ResponseBody
    void deleteExercise(@PathVariable String id) throws InvalidIdException {
        exerciseService.deleteExercise(id);
    }

    @PreAuthorize("hasRole('THERAPIST')")
    @RequestMapping(value = ExerciseRoutes.UPDATE, method = RequestMethod.PUT)
    public @ResponseBody
    Exercise updateExercise(@RequestBody Exercise sentExercise, @PathVariable String id) throws InvalidIdException {
        Exercise updatedExercise = exerciseService.updateExercise(sentExercise, id);
        return updatedExercise;
    }
}
