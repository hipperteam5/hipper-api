/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Exceptions.InvalidCredentialsException;
import Exceptions.InvalidIdException;
import Exceptions.InvalidJsonException;
import Exceptions.UsernameUnavailableException;
import Routes.AdminRoutes;
import com.auth0.jwt.internal.com.fasterxml.jackson.core.type.TypeReference;
import com.auth0.jwt.internal.com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import models.Admin;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import repositories.AdminRepository;
import services.AdminService;

/**
 *
 * @author yorin
 */
@Controller
public class AdminController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private AdminRepository adminRepository;

    Logger log = Logger.getLogger(AdminController.class);

    @RequestMapping(value = AdminRoutes.CREATE, method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    Admin createAdmin(@RequestBody String sentAdmin) throws IOException, UsernameUnavailableException, InvalidJsonException {
        ObjectMapper mapper = new ObjectMapper();
        TypeReference reference = new TypeReference<HashMap<String, String>>() {
        };
        Map<String, String> map = new HashMap<String, String>();
        try {
            map = mapper.readValue(sentAdmin, new TypeReference<HashMap<String, String>>() {
            });
        } catch (Exception e) {
            throw new InvalidJsonException();
        }
        Admin admin = adminService.createAdmin(map.get("username"), map.get("password"));
        return admin;
    }

    @RequestMapping(value = AdminRoutes.BASE, method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    List<Admin> allAdmins() {
        log.log(Priority.INFO, "GETTING ALL USERS");
        List<Admin> admins = adminRepository.findAll();
        return admins;
    }

    @RequestMapping(value = AdminRoutes.BY_ID, method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    Admin getAdminById(@PathVariable String id) throws InvalidIdException, InvalidCredentialsException, IOException {
        log.info("GETTING USER WITH ID: " + id);
        Admin admin = adminService.findAdminById(id);
        return admin;
    }

    /*
     @param id of the admin that needs to be deleted
     @return id of the deleted admin
     @throws InvalidIdException
     */
    @RequestMapping(value = AdminRoutes.DELETE, method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    String deleteAdmin(@PathVariable String id) throws InvalidIdException {
        String deletedTherapistId = adminService.deleteAdmin(id);
        return deletedTherapistId;
    }

    @RequestMapping(value = AdminRoutes.CHECKNAME, method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    boolean checkNameAvailable(@RequestBody String name) throws InvalidJsonException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> map = new HashMap<String, String>();
        try {
            map = mapper.readValue(name, new TypeReference<HashMap<String, String>>() {
            });
        } catch (Exception e) {
            throw new InvalidJsonException();
        }
        return adminService.usernameAvailable(map.get("username"));
    }

    //update Admin
    //update Admin password
}
