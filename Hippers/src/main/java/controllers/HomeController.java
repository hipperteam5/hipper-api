/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Exceptions.InvalidCredentialsException;
import Exceptions.InvalidIdException;
import Routes.MainRoutes;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import models.Admin;
import models.Patient;
import models.Therapist;
import models.Token;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import repositories.AdminRepository;
import repositories.PatientRepository;
import repositories.TherapistRepository;

/**
 *
 * @author yorin
 */
@Controller
public class HomeController {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private TherapistRepository therapistRepository;
    
    @Autowired
    private PatientRepository patientRepository;

    Logger log = Logger.getLogger(HomeController.class);

    @RequestMapping("/")
    public @ResponseBody
    Object home(ModelMap model) {
        log.info("API HOME");
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("name", "Hipper API");
        builder.add("version", "0.1.0");
        JsonObject result = builder.build();
        return result;
    }

    @RequestMapping(value = MainRoutes.ME, method = RequestMethod.GET)
    public @ResponseBody
    Object getCurrentUser() throws InvalidCredentialsException, InvalidIdException {
        log.info("GETTING CURRENT USER");
        Token activeUser = (Token) SecurityContextHolder.getContext().getAuthentication().getDetails();
        String typeOfAccount = activeUser.getRoles().get(0);
        log.info(typeOfAccount);
        log.info(activeUser.getUser_id());
        if (typeOfAccount.equals("ADMIN")) {
            Admin admin = adminRepository.findById(activeUser.getUser_id());
            return admin;
        }
        if (typeOfAccount.equals("THERAPIST")) {
            Therapist therapist = therapistRepository.findById(activeUser.getUser_id());
            return therapist;
        }
        if (typeOfAccount.equals("PATIENT")) {
            Patient patient = patientRepository.findById(activeUser.getUser_id());
            return patient;
        }
        return null;
    }

}
