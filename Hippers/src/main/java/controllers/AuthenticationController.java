/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Exceptions.InvalidCredentialsException;
import Exceptions.InvalidIdException;
import Routes.MainRoutes;
import configuration.UserConstants;
import models.Admin;
import models.Credentials;
import models.JwtTokenResult;
import models.Patient;
import models.Therapist;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import services.AuthenticationService;
import services.PatientService;
import services.TokenService;

/**
 *
 * @author mats
 */
@Controller
public class AuthenticationController {

    Logger log = Logger.getLogger(AuthenticationController.class);

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private TokenService tokenService;
    
    @Autowired
    private PatientService patientService;

    @RequestMapping(value = MainRoutes.AUTHENTICATE, method = RequestMethod.POST)
    public @ResponseBody
    JwtTokenResult authenticate(@RequestBody Credentials credentials) throws InvalidCredentialsException, Exception {
        log.info("AUTHENTICATING");
        switch (credentials.getRole()) {
            case UserConstants.ADMIN_ROLE:
                log.info("ADMIN");
                Admin admin = authenticationService.authenticateAdmin(credentials.getUsername(), credentials.getPassword());
                return tokenService.createAdminToken(admin);
            case UserConstants.THERAPIST_ROLE:
                log.info("THERAPIST");
                Therapist therapist = authenticationService.authenticateTherapist(credentials.getUsername(), credentials.getPassword());
                return tokenService.createTherapistToken(therapist);
            default:
                log.info("SHIT?");
                throw new InvalidCredentialsException();
        }
    }
    
    @RequestMapping(value = MainRoutes.AUTHENTICATE_PATIENT, method = RequestMethod.POST)
    public @ResponseBody JwtTokenResult authenticatePatient(@PathVariable String id) throws InvalidIdException, Exception {
        if(id.equals("mats")){
            id = "546489c5d4bbafd1872bdc25";
        }
        
        Patient patient = patientService.findById(id);
        
        return tokenService.createPatientToken(patient);
    }
}
