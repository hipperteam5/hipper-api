/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Exceptions.InvalidIdException;
import Exceptions.InvalidJsonException;
import Exceptions.UsernameUnavailableException;
import Routes.TherapistRoutes;
import com.auth0.jwt.internal.com.fasterxml.jackson.core.type.TypeReference;
import com.auth0.jwt.internal.com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import models.Therapist;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import repositories.TherapistRepository;
import services.TherapistService;

/**
 *
 * @author yorin
 */
@Controller
public class TherapistController {

    @Autowired
    TherapistService therapistService;

    @Autowired
    TherapistRepository therapistRepository;

    Logger log = Logger.getLogger(TherapistController.class);

    @RequestMapping(value = TherapistRoutes.CREATE, method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    Therapist createTherapist(@RequestBody String sentTherapist) throws IOException, UsernameUnavailableException, InvalidJsonException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> map = new HashMap<String, String>();
        try {
            map = mapper.readValue(sentTherapist, new TypeReference<HashMap<String, String>>() {
            });
        } catch (Exception e) {
            throw new InvalidJsonException();
        }
        Therapist therapist = therapistService.createTherapist(map.get("username"), map.get("password"), map.get("firstName"),
                map.get("lastName"), map.get("phoneNr"), map.get("therapistType"));
        return therapist;
    }

    @RequestMapping(value = TherapistRoutes.UPDATE, method = RequestMethod.PUT)
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    Therapist updateTherapist(@RequestBody Therapist sentTherapist, @PathVariable String id) throws InvalidIdException {
        Therapist therapist = therapistService.updateTherapist(sentTherapist, id);
        return therapist;
    }

//    @RequestMapping(value = TherapistRoutes.UPDATE, method = RequestMethod.PUT)
//    @PreAuthorize("hasRole('ADMIN')")
//    public @ResponseBody
//    Therapist updateTherapistPassword(@RequestBody String sentTherapist, @PathVariable String id) throws IOException, InvalidIdException, InvalidJsonException {
//        ObjectMapper mapper = new ObjectMapper();
//        Map<String, String> map = new HashMap<String, String>();
//        try {
//            map = mapper.readValue(sentTherapist, new TypeReference<HashMap<String, String>>() {
//            });
//        } catch (Exception e) {
//            throw new InvalidJsonException();
//        }
//            Therapist therapist = therapistService.updateTherapistPassword( sentTherapist,id, map.get("password"));
//            //  Therapist therapist = therapistService.updateTherapist(sentTherapist, id,map.get("username"));
//            return therapist;
//        }
//    
    @RequestMapping(value = TherapistRoutes.BASE, method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    List<Therapist> allTherapists() {
        log.info("GETTING ALL USERS");
        List<Therapist> therapists = therapistRepository.findAll();
        return therapists;
    }

    @RequestMapping(value = TherapistRoutes.BY_ID, method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    Therapist getTherapistById(@PathVariable String id) throws InvalidIdException {
        Therapist therapist = therapistService.findTherapistById(id);
        return therapist;
    }

    @RequestMapping(value = TherapistRoutes.CHECKNAME, method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    boolean checkNameAvailable(@RequestBody String name) throws InvalidJsonException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> map = new HashMap<String, String>();
        try {
            map = mapper.readValue(name, new TypeReference<HashMap<String, String>>() {
            });
        } catch (Exception e) {
            throw new InvalidJsonException();
        }
        return therapistService.usernameAvailable(map.get("username"));
    }

    /*
     @param id of the therpist that needs to be deleted
     @return id of the deleted therapist
     @throws InvalidIdException
     */
    @RequestMapping(value = TherapistRoutes.DELETE, method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('ADMIN')")
    public @ResponseBody
    String deleteTherapist(@PathVariable String id) throws InvalidIdException {
        String deletedId = therapistService.deleteTherapist(id);
        return deletedId;

    }
}
