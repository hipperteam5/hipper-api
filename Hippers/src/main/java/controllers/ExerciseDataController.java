/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Exceptions.InvalidIdException;
import models.ExerciseData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import repositories.ExerciseDataRepository;
import services.ExerciseDataService;
import services.FileManager;

/**
 *
 * @author yorin
 */
@Controller
public class ExerciseDataController {

    FileManager fileManager = new FileManager("/home/yorin/files/Knie strekken.csv");

    @Autowired
    ExerciseDataService exerciseDataService;

    @Autowired
    ExerciseDataRepository exerciseDataRepository;

    Logger log = Logger.getLogger(ExerciseDataController.class);

    @RequestMapping(value = "/read", method = RequestMethod.GET)
    public @ResponseBody
    String readCSVToDB() {
        fileManager.read();
        ExerciseData exerciseData = new ExerciseData(fileManager.getX(), fileManager.getY(), fileManager.getZ(),
                fileManager.getTimeStamp(), fileManager.getRunId());
        exerciseDataRepository.save(exerciseData);
        log.info(fileManager.checkFile());
        if (fileManager.checkFile() > 0) {
            return "Succes";
        } else {
            return "FAILUREEEE";
        }
    }

    @RequestMapping(value = "/secured/count/{id}", method = RequestMethod.GET)
    public @ResponseBody
    int repsForJesus(@PathVariable String id) throws InvalidIdException {
        log.info(id);
        int result = exerciseDataService.countReps(id);

        if (result > 0) {
            return result;
        } else {
            return 000000000;
        }
    }

}
