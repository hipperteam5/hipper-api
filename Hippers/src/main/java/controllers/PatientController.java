package controllers;

import Exceptions.InvalidIdException;
import Exceptions.InvalidJsonException;
import Exceptions.AccessDeniedException;
import Routes.PatientRoutes;
import java.util.List;
import models.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import services.PatientService;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author yorin
 */
@Controller
public class PatientController {

    Logger log = Logger.getLogger(PatientController.class);

    @Autowired
    private PatientService patientService;

    @PreAuthorize("hasRole('THERAPIST')")
    @RequestMapping(value = PatientRoutes.CREATE, method = RequestMethod.POST)
    public @ResponseBody
    Object createPatient(@RequestBody Patient sentPatient) throws InvalidJsonException {
        log.info(sentPatient.toString());
        Patient createdPatient = patientService.createPatient(sentPatient);
        log.info(createdPatient);
        return createdPatient;
    }

    @PreAuthorize("hasRole('THERAPIST')")
    @RequestMapping(value = PatientRoutes.BASE, method = RequestMethod.GET)
    public @ResponseBody
    List<Patient> findByTherapist() throws InvalidIdException {
        List<Patient> patients = patientService.findPatientsByTherapist();
        return patients;
    }

    @PreAuthorize("hasRole('THERAPIST')")
    @RequestMapping(value = PatientRoutes.BY_ID, method = RequestMethod.GET)
    public @ResponseBody
    Patient findById(@PathVariable String id) throws InvalidJsonException, InvalidIdException, AccessDeniedException {
        Patient patient = patientService.findByIdForTherapist(id);
        return patient;
    }

    // update patient
    @PreAuthorize("hasRole('THERAPIST')")
    @RequestMapping(value = PatientRoutes.UPDATE, method = RequestMethod.PUT)
    public @ResponseBody
    Patient updatePatient(@RequestBody Patient sentPatient, @PathVariable String id) throws InvalidIdException {
        Patient patient = patientService.updatePatient(sentPatient, id);
        return patient;
    }
}
