package controllers;

import Exceptions.InvalidIdException;
import Exceptions.AccessDeniedException;
import Routes.AgendaRoutes;
import java.util.List;
import models.Agenda;
import models.SentEvent;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import services.AgendaService;

/**
 *
 * @author Maarten Petit
 */
@Controller
public class AgendaController {

    @Autowired
    private AgendaService agendaService;

    Logger log = Logger.getLogger(AgendaController.class);

    @PreAuthorize("hasRole('THERAPIST')")
    @RequestMapping(value = AgendaRoutes.BY_PATIENT_ID, method = RequestMethod.PUT)
    public @ResponseBody
    Agenda addEvents(@RequestBody List<SentEvent> sentEvents, @PathVariable String patientId) throws InvalidIdException, AccessDeniedException {
        Agenda agenda = agendaService.addEvents(patientId, sentEvents);
        return agenda;
    }

    @PreAuthorize("hasRole('THERAPIST')")
    @RequestMapping(value = AgendaRoutes.BY_PATIENT_ID, method = RequestMethod.GET)
    public @ResponseBody
    Agenda getAgendaByPatientId(@PathVariable String patientId) throws InvalidIdException, AccessDeniedException {
        Agenda agenda = agendaService.findAgendaByPatientIdForTherapist(patientId);
        return agenda;
    }

    @PreAuthorize("hasRole('THERAPIST')")
    @RequestMapping(value = AgendaRoutes.DELETE, method = RequestMethod.DELETE)
    public @ResponseBody
    Agenda deleteEvent(@PathVariable String patientId, @PathVariable String assignedExerciseId) throws InvalidIdException, AccessDeniedException {
        Agenda agenda = agendaService.deleteEvent(patientId, assignedExerciseId);
        return agenda;
    }
}
