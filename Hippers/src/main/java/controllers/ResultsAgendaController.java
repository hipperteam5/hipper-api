package controllers;

import Exceptions.InvalidIdException;
import Exceptions.AccessDeniedException;
import Routes.ResultsAgendaRoutes;
import java.text.ParseException;
import models.ResultsAgenda;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import services.ResultsAgendaService;

/**
 *
 * @author Maarten Petit
 */
@Controller
public class ResultsAgendaController {

    @Autowired
    private ResultsAgendaService resultsAgendaService;

    @PreAuthorize("hasRole('THERAPIST')")
    @RequestMapping(value = ResultsAgendaRoutes.BY_PATIENT_ID, method = RequestMethod.GET)
    public @ResponseBody
    ResultsAgenda getResultsAgenda(@PathVariable String patientId) throws InvalidIdException, AccessDeniedException, ParseException {
        ResultsAgenda resultsAgenda = resultsAgendaService.getResultsAgenda(patientId);
        return resultsAgenda;
    }

}
