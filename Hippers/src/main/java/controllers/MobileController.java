/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Exceptions.InvalidIdException;
import Routes.MobileRoutes;
import java.util.List;
import models.Agenda;
import models.Event;
import models.EventList;
import models.Token;
import models.AssignedExercise;
import models.Exercise;
import models.mobile.MobileEvent;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import services.AgendaService;
import services.AssignedExerciseService;
import services.ExerciseService;

/**
 *
 * @author mats
 */
@Controller
public class MobileController {
    @Autowired
    private AgendaService agendaService;
    
    @Autowired
    private AssignedExerciseService AEservice;
    
    @Autowired
    private ExerciseService Eservice;
    
     Logger log = Logger.getLogger(MobileController.class);
    /**
     * 
     * @return EventList of logged in patient
     * @throws InvalidIdException 
     */
    @PreAuthorize("hasRole('PATIENT')")
    @RequestMapping(value = MobileRoutes.AGENDA, method = RequestMethod.GET)
    public @ResponseBody
    EventList getAgenda() throws InvalidIdException {
        log.info("Getting EventList");
        Token activeUser = (Token) SecurityContextHolder.getContext().getAuthentication().getDetails();
        String patientId = activeUser.getUser_id();
        Agenda agenda = agendaService.findAgendaByPatientId(patientId);
        log.info("Found Agenda");
        return new EventList(agenda.getEvents());
    }
    
    /**
     * @param AEid --> Assigned Exercise Id
     * @return MobileEvent
     * @throws InvalidIdException 
     */
    @PreAuthorize("hasRole('PATIENT')")
    @RequestMapping(value = MobileRoutes.EVENT, method = RequestMethod.GET)
    public @ResponseBody
    MobileEvent getEvent(@PathVariable String id) throws InvalidIdException {
//         Get active user
        Token activeUser = (Token) SecurityContextHolder.getContext().getAuthentication().getDetails();
        String patientId = activeUser.getUser_id();
        
        log.info("GET AGENDA");
        //Get patient agenda
        Agenda agenda = agendaService.findAgendaByPatientId(patientId);

        //Get assigned event from agenda
        Event event = agenda.getEventByAssignedExerciseId(id);
                log.info("Get Exercuse");
        //Get assigned exercise
        AssignedExercise assigned = AEservice.getAssignedExerciseById(id);
        
        //Get exercise base
        Exercise exercise = Eservice.findExerciseById(assigned.getExerciseId());
        
        //Create MobileEvent
        MobileEvent obj = new MobileEvent(exercise, assigned, event);
        
        //Return MobileEvent        
        return obj;
    }
}
