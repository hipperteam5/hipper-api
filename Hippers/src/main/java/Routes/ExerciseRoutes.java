/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Routes;

/**
 *
 * @author Maarten
 */
public class ExerciseRoutes {

    public static final String BASE = MainRoutes.SECURED + "/exercises";
    public static final String CREATE = BASE;
    public static final String BY_ID = BASE + "/{id}";
    public static final String DELETE = BY_ID;
    public static final String UPDATE = BY_ID;
    public static final String CHECKNAME = BASE + "/{name}";
}
