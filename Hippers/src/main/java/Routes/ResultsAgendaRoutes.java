package Routes;

/**
 *
 * @author Maarten Petit
 */
public class ResultsAgendaRoutes {

    public static final String BASE = MainRoutes.SECURED + "/resultsagendas";
    public static final String BY_PATIENT_ID = BASE + "/{patientId}";

}
