/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Routes;

/**
 *
 * @author mats
 */
public class MainRoutes {
    public static final String SECURED = "/secured";
    public static final String AUTHENTICATE = "/authenticate";
    public static final String AUTHENTICATE_PATIENT = AUTHENTICATE + "/{id}";
    public static final String ME = SECURED + "/me";
}
