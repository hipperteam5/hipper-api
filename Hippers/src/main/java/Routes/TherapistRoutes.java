/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Routes;

/**
 *
 * @author mats
 */
public class TherapistRoutes {
    /*
     Therapists
     */

    public static final String BASE = MainRoutes.SECURED + "/therapists"; // /therapists
    public static final String CREATE = BASE;
    public static final String BY_ID = BASE + "/{id}"; // /therapists/{id}
    public static final String DELETE = BY_ID;
    public static final String UPDATE = BY_ID;
    public static final String CHECKNAME = BASE + "/check";

   // public static final String 
}
