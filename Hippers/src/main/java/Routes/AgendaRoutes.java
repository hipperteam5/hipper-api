/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Routes;

/**
 *
 * @author Maarten
 */
public class AgendaRoutes {

    public static final String BASE = MainRoutes.SECURED + "/agendas";
    public static final String BY_PATIENT_ID = BASE + "/{patientId}";
    public static final String DELETE = BY_PATIENT_ID + "/{assignedExerciseId}";
}
