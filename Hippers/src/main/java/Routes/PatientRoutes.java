/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Routes;

/**
 *
 * @author yorin
 */
public class PatientRoutes {

    public static final String BASE = MainRoutes.SECURED + "/patients"; //patients of therapist
    public static final String CREATE = BASE;
    public static final String BY_ID = BASE + "/{id}"; //patient by Id
    public static final String UPDATE = BY_ID;
    public static final String CHECKNAME = BASE + "/{name}";
    public static final String setActive = BY_ID + "/setActive";
    public static final String setInActive = BY_ID + "/setInActive";
}
