/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Routes;

/**
 *
 * @author mats
 */
public class MobileRoutes {
     public static final String BASE = MainRoutes.SECURED + "/mobile";
     
     public static final String AGENDA = BASE + "/agenda";
     public static final String EVENT = BASE + "/events/{id}";
}
