package Routes;

/**
 *
 * @author Maarten Petit
 */
public class WeekGraphRoutes {

    public static final String BASE = MainRoutes.SECURED + "/weekresults";
    public static final String BY_PATIENT_ID = BASE + "/{patientId}";
    public static final String BY_YEAR = BY_PATIENT_ID + "/{year}";
    public static final String BY_WEEKNUMBER = BY_YEAR + "/{weeknumber}";

}
