/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

import configuration.ErrorConstants;

/**
 *
 * @author mats
 */
public class InvalidIdException extends Exception {

    private final String message;
    private final int code;
    private String invalidId;

    public InvalidIdException() {
        this.message = ErrorConstants.INVALID_ID_MESSAGE;
        this.code = ErrorConstants.INVALID_ID_CODE;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public String getUnavailableUsername() {
        return invalidId;
    }

    public void setUnavailableUsername(String invalidId) {
        this.invalidId = invalidId;
    }
}
