/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

import configuration.ErrorConstants;

/**
 *
 * @author mats
 */
public class UsernameUnavailableException extends Exception {

    private final String message;
    private final int code;
    private String unavailableUsername;

    public UsernameUnavailableException() {
        this.message = ErrorConstants.USERNAME_EXISTS_MESSAGE;
        this.code = ErrorConstants.USERNAME_EXISTS_CODE;
    }

    public int getCode() {
        return code;
    }

    public String getUnavailableUsername() {
        return unavailableUsername;
    }

    public void setUnavailableUsername(String unavailableUsername) {
        this.unavailableUsername = unavailableUsername;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
