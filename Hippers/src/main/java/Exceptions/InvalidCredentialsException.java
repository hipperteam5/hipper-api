/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

import configuration.ErrorConstants;

/**
 *
 * @author mats
 */
public class InvalidCredentialsException extends Exception {

    private final String message;
    private final int code;

    public InvalidCredentialsException() {
        this.message = ErrorConstants.INVALID_CREDENTIALS_MESSAGE;
        this.code = ErrorConstants.INVALID_CREDENTIALS_CODE;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
