/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

import configuration.ErrorConstants;

/**
 *
 * @author mats
 */
public class AccessDeniedException extends Exception {

    private final String message;
    private final int code;

    public AccessDeniedException() {
        this.message = ErrorConstants.NO_ACCESS_MESSAGE;
        this.code = ErrorConstants.NO_ACCESS_CODE;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
