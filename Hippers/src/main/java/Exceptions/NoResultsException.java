/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

import configuration.ErrorConstants;

/**
 *
 * @author Maarten Petit
 */
public class NoResultsException extends Exception {

    private final String message;
    private final int code;

    public NoResultsException() {
        this.message = ErrorConstants.NO_RESULTS_MESSAGE;
        this.code = ErrorConstants.NO_RESULTS_CODE;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
