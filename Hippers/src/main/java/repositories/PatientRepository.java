/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.util.List;
import models.Patient;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author yorin
 */
@Repository
public interface PatientRepository extends MongoRepository<Patient, String> {
    public List<Patient> findByTherapistId(String id);
    public Patient findById(String id);

}
