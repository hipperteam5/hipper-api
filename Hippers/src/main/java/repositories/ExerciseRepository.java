/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import models.Exercise;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Maarten
 */
@Repository
public interface ExerciseRepository extends MongoRepository<Exercise, String> {

    public Exercise findByExerciseName(String exerciseName);

}
