/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import models.ExerciseData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author yorin
 */
@Repository
public interface ExerciseDataRepository extends MongoRepository<ExerciseData, String> {

    public ExerciseData findById(String id);

    public ExerciseData findByAssignedExerciseId(String assignedExerciseId);
}
