/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import models.Hash;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Yorin
 */
@Repository
public interface HashRepository extends MongoRepository<Hash, String> {

    public Hash findByAdminId(String id);

    public Hash findByTherapistId(String id);

    public Hash findByPatientId(String id);
}
