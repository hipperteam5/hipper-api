/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import models.Therapist;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author yorin
 */
@Repository
public interface TherapistRepository extends MongoRepository<Therapist, String> {

    public Therapist findByUsername(String username);

    public Therapist findById(String Id);
}
