/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.util.List;
import models.AssignedExercise;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Maarten Petit
 */
@Repository
public interface AssignedExerciseRepository extends MongoRepository<AssignedExercise, String> {

    public List<AssignedExercise> findByPatientId(String patientId);

    public List<AssignedExercise> findByExerciseId(String exerciseId);
    
    public AssignedExercise findOne(String id);
}
