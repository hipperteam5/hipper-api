/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import models.ResultsAgenda;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Maarten
 */
@Repository
public interface ResultsAgendaRepository extends MongoRepository<ResultsAgenda, String> {

    public ResultsAgenda findByPatientId(String patientId);

}
