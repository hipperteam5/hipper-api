/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import models.Admin;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author yorin
 */
@Repository
public interface AdminRepository extends MongoRepository<Admin, String> {

    public Admin findByUsername(String username);

    public Admin findById(String Id);

}
