/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration.Authorization;

import com.auth0.jwt.Algorithm;
import com.auth0.jwt.ClaimSet;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JwtProxy;
import com.auth0.jwt.JwtSigner;
import com.auth0.jwt.PayloadHandler;
import com.auth0.jwt.internal.org.apache.commons.codec.binary.Base64;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author mats
 */
public class JWT implements JwtProxy {

    private Logger log = Logger.getLogger(JWT.class);
    // the payload identifier in the JSON object

    private static final String PAYLOAD_ID = "JABADABADOE";
    private PayloadHandler payloadHandler;

    @Override
    public void setPayloadHandler(PayloadHandler payloadHandler) {
        this.payloadHandler = payloadHandler;
    }

    public PayloadHandler getPayloadHandler() {
        return payloadHandler;
    }

    /**
     * Create a JSON web token by serializing a java object
     *
     * @param algorithm
     * @param obj
     * @param secret
     * @param claimSet
     * @return
     */
    @Override
    public String encode(Algorithm algorithm, Object obj, String secret,
            ClaimSet claimSet) throws Exception {

        JwtSigner jwtSigner = new JwtSigner();
        String payload = getPayloadHandler().encoding(obj);

        return jwtSigner.encode(algorithm, payload, PAYLOAD_ID, secret, claimSet);
    }

    /**
     * Verify a JSON web token and return the object serialized in the JSON
     * payload
     *
     * @param algorithm
     * @param token
     * @param secret
     * @return
     * @throws java.lang.Exception
     */
    @Override
    public Object decode(Algorithm algorithm, String token, String secret)
            throws Exception {
        log.info("Voor verifier");
        JWTVerifier jwtVerifier = new JWTVerifier(Base64.encodeBase64String(secret.getBytes()));
        log.info("MAPPER");
        Map<String, Object> verify = jwtVerifier.verify(token);
        log.info("payload");
        String payload = (String) verify.get(PAYLOAD_ID);

        return getPayloadHandler().decoding(payload);
    }
}
