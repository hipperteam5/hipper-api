/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration.Authorization;

import com.auth0.jwt.PayloadHandler;
import com.auth0.jwt.internal.com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author mats
 */
public class JwtPayloadHandler implements PayloadHandler {

    @Override
    public String encoding(Object payload) throws Exception {
        return new ObjectMapper().writeValueAsString(payload);
    }

    @Override
    public Object decoding(String payload) throws Exception {
        return payload;
    }

}
