/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration.Authorization;

import com.auth0.jwt.Algorithm;
import com.google.gson.Gson;
import configuration.AppConstants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import models.Token;
import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author mats
 */
public class TokenAuthentication implements Authentication {

    private Logger log = Logger.getLogger(TokenAuthentication.class);
    private String token;
    private Token user;
    private boolean authenticated = false;
    private List<GrantedAuthority> authorities = new ArrayList();

    public TokenAuthentication(String token) throws Exception {
        this.token = token;
        log.info(token);
        doAuthentication();
    }

    private void doAuthentication() throws Exception {

        JWT jwt = new JWT();
        jwt.setPayloadHandler(new JwtPayloadHandler());
        String decoded = (String) jwt.decode(Algorithm.HS256, token, AppConstants.SECRET);
        log.info(decoded);
        //--> Object maken
        Gson gson = new Gson();
        this.user = gson.fromJson(decoded, Token.class);
        List<String> roles = user.getRoles();
        for (String s : roles) {
            log.info(s);
            GrantedAuthority nieuw = new ToegewezenRecht(s);
            authorities.add(nieuw);
        }
        this.setAuthenticated(true);

    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

    @Override
    public Object getDetails() {
        return user;
    }

    @Override
    public Object getPrincipal() {
        return user;
    }

    @Override
    public boolean isAuthenticated() {
        return this.authenticated;
    }

    @Override
    public void setAuthenticated(boolean bln) throws IllegalArgumentException {
        this.authenticated = bln;
    }

    @Override
    public String getName() {
        return user.getUsername();
    }

}
