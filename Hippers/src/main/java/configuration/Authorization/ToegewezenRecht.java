/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration.Authorization;

import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author mats
 */
public class ToegewezenRecht implements GrantedAuthority {

    private String authority;

    public ToegewezenRecht(String recht) {
        this.authority = recht;
    }

    @Override
    public String getAuthority() {
        return this.authority;
    }

}
