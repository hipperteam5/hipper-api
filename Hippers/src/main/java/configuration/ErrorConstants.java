/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration;

/**
 *
 * @author mats
 */
public class ErrorConstants {

    /**
     * 1000 Update/save errors
     */
    //Duplicate Key
    public static final int DUP_KEY_CODE = 1001;
    public static final String DUP_KEY_MESSAGE = "Entity must be unique";

    public static final String USERNAME_EXISTS_MESSAGE = "Username already exists";
    public static final int USERNAME_EXISTS_CODE = 1002;

    //Id doesn't exist
    public static final String INVALID_ID_MESSAGE = "Id doesn't exist";
    public static final int INVALID_ID_CODE = 1003;

    //Json is incorrect
    public static final String INVALID_JSON_MESSAGE = "Invalid Json";
    public static final int INVALID_JSON_CODE = 1004;

    //Weeknumber is incorrect (<0 or >52).
    public static final String INVALID_WEEKNUMBER_MESSAGE = "Invalid Weeknumber";
    public static final int INVALID_WEEKNUMBER_CODE = 1005;

    //No results for given weeknumber
    public static final String NO_RESULTS_MESSAGE = "No Results Available";
    public static final int NO_RESULTS_CODE = 1006;

    /**
     * 2000 Authentication/Authorization ERRORS
     */
    public static final String INVALID_CREDENTIALS_MESSAGE = "Invalid Credentials";
    public static final int INVALID_CREDENTIALS_CODE = 2001;

    public static final String NO_ACCESS_MESSAGE = "No access to this resource";
    public static final int NO_ACCESS_CODE = 2002;

    public static final int INVALID_TOKEN_CODE = 2003;
    public static final String INVALID_TOKEN_MESSAGE = "Invalid Access Token";
}
