    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration.Filters;

import configuration.Authorization.TokenAuthentication;
import com.auth0.jwt.Algorithm;
import com.auth0.jwt.internal.com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.mongodb.util.JSON;
import configuration.AppConstants;
import configuration.Authorization.JWT;
import configuration.Authorization.JwtPayloadHandler;
import controllers.ExceptionController;
import java.io.IOException;
import java.util.Enumeration;
import java.util.logging.Level;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.Token;
import net.minidev.json.JSONValue;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 *
 * @author mats
 */
public class HipperSecurityFilter extends OncePerRequestFilter {

    private Logger log = Logger.getLogger(HipperSecurityFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws ServletException, IOException {
        String methode = req.getMethod();
        if (methode.equals("OPTIONS")) {
            log.info(methode);
            chain.doFilter(req, res);
        } else {
            /*
             This only happens if request method is not OPTIONS
             */
            String token = req.getHeader("hipperaccesstoken");

            if (token != null) {

                try {
                    log.info(token);
                    //Token Decoden
                    // Authenticate the user

                    Authentication authentication = new TokenAuthentication(token);
                    SecurityContext securityContext = SecurityContextHolder.getContext();
                    securityContext.setAuthentication(authentication);

                    // Create a new session and add the security context.
                    HttpSession session = req.getSession(true);
                    session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);

                    //Aan request koppelen
                    chain.doFilter(req, res);
                } catch (Exception ex) {
                    log.warn("BAD TOKEN!");
                    res.sendError(res.SC_UNAUTHORIZED);
                }

            } else {
              
                log.warn("NO TOKEN!");
                res.sendError(res.SC_UNAUTHORIZED);
            }
        }
    }
}
