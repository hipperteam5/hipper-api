/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration;

/**
 *
 * @author mats
 */
public class UserConstants {
    public static final int ADMIN_ROLE = 1;
    public static final int THERAPIST_ROLE = 2;
    public static final int PATIENT_ROLE = 3;
}
